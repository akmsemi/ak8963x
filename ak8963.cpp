#include "ak8963.h"

// REGISTER MAP
#define AK8963_REG_WIA             0x00 // Company Name (data=48h)
#define AK8963_REG_INFO            0x01 // Device Code
#define AK8963_REG_ST1             0x02
#define AK8963_REG_HXL             0x03
#define AK8963_REG_HXH             0x04
#define AK8963_REG_HYL             0x05
#define AK8963_REG_HYH             0x06
#define AK8963_REG_HZL             0x07
#define AK8963_REG_HZH             0x08
#define AK8963_REG_ST2             0x09
#define AK8963_REG_CNTL1           0x0A
#define AK8963_REG_CNTL2           0x0B
#define AK8963_REG_ASTC            0x0C
#define AK8963_REG_TS1             0x0D
#define AK8963_REG_TS2             0x0E
#define AK8963_REG_I2CDIS          0x0F
#define AK8963_REG_ASAX            0x10
#define AK8963_REG_ASAY            0x11
#define AK8963_REG_ASAZ            0x12
#define AK8963_REG_RSV             0x13

// BITS in CNTL1 register
#define AK8963_CNTL1_MODE_PDN      0x00 // POWER DOWN MODE
#define AK8963_CNTL1_MODE_SNG      0x01 // SINGLE MEASUREMENT MODE
#define AK8963_CNTL1_MODE_CNT1     0x02 // CONTINUOUS MEASUREMENT MODE 1
#define AK8963_CNTL1_MODE_CNT2     0x06 // CONTINUOUS MEASUREMENT MODE 2
#define AK8963_CNTL1_MODE_EXT      0x04 // EXTERNAL TRIGGER MEASUREMENT MODE
#define AK8963_CNTL1_MODE_TEST     0x08 // SELF-TEST MODE
#define AK8963_CNTL1_MODE_FUSE_ROM 0x0F // FUSE ROM ACCESS MODE
#define AK8963_CNTL1_OUTPUT_14BIT  0x00
#define AK8963_CNTL1_OUTPUT_16BIT  0x10

#define AK8963_BIT_MASK_DRDY       0x01
#define AK8963_BIT_MASK_DOR        0x02
#define AK8963_BIT_MASK_HOFL       0x08
#define AK8963_BIT_MASK_MODE       0x0F

#define AK8963_VAL_WIA             0x48
#define AK8963_VAL_INFO_1          0x99
#define AK8963_VAL_INFO_2          0x9A
#define AK8963_VAL_INFO_3          0x9B
#define AK8963_VAL_INFO_4          0x9C
#define AK8963_VAL_I2CDIS          0x1B
#define AK8963_VAL_SOFT_RESET      0x01

#define AK8963_BUF_LENGTH_ASA      3
#define AK8963_BUF_LENGTH_BDATA    8
#define AK8963_SENSITIVITY         (0.15)   // uT/LSB
#define AK8963_DENOMINATOR         256.0
#define AK8963_WAIT_POWER_DOWN_US  100
#define AK8963_WAIT_SOFT_RESET_US  1000

AK8963::AK8963(){
    i2c = NULL;
    spi = NULL;
    cs = NULL;
}

AK8963::~AK8963(){
    if(i2c) delete i2c;
    if(spi) delete spi;
    if(cs) delete cs;
}

void AK8963::init_sub() {
    // software reset
    char buf = AK8963_VAL_SOFT_RESET;
    AK8963::write(AK8963_REG_CNTL2, &buf, AKMECOMPASS_LEN_ONE_BYTE);
    wait_us(AK8963_WAIT_SOFT_RESET_US);
    
    getSensitivityAdjustment(&sensitivityAdjustment);
}

void AK8963::init(I2C *conn, SlaveAddress addr, DeviceId id) {
    if(i2c) delete i2c;
    if(spi) delete spi;
    if(cs) delete cs;
    i2c = conn;
    slaveAddress = addr;
    deviceId = id;
    
    init_sub();
}

void AK8963::init(SPI *_spi, DigitalOut *_cs, DeviceId id) {
    if(i2c) delete i2c;
    if(spi) delete spi;
    if(cs) delete cs;
    spi=_spi;
    cs=_cs;
    cs->write(1);
    deviceId = id;
        
    init_sub();
    
    char buf = AK8963_VAL_I2CDIS;
    AK8963::write(AK8963_REG_I2CDIS, &buf, AKMECOMPASS_LEN_ONE_BYTE);
}

AkmECompass::Status AK8963::checkConnection() {
    AkmECompass::Status status = AkmECompass::SUCCESS;
    
    // Gets the WIA register value.
    char wiaValue = 0;
    if ((status=AK8963::read(AK8963_REG_WIA, &wiaValue, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        return status;
    }
    // Checks the obtained value equals to the supposed value.
    if (wiaValue != AK8963_VAL_WIA) {
        return AkmECompass::ERROR;
    }

    // Gets the INFO register value.
    char infoValue = 0;
    if ((status=AK8963::read(AK8963_REG_INFO, &infoValue, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        return status;
    }
    // Checks the obtained value equals to the supposed value.
    if ( infoValue != AK8963_VAL_INFO_1 &&
         infoValue != AK8963_VAL_INFO_2 &&
         infoValue != AK8963_VAL_INFO_3 &&
         infoValue != AK8963_VAL_INFO_4 ) {
        return AkmECompass::ERROR;
    }
    
    return status;
}

AkmECompass::Status AK8963::isDataReady() {
    AkmECompass::Status status = AkmECompass::ERROR;
    
    // Gets the ST1 register value.
    char st1Value = 0;
    if ((status=AK8963::read(AK8963_REG_ST1, &st1Value, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        // I2C read failed.
        return status;
    }

    // Sets a return status corresponds to the obtained value.    
    if ((st1Value & AK8963_BIT_MASK_DRDY) > 0) {
        status = AK8963::DATA_READY;
    } else {
        status = AK8963::NOT_DATA_READY;
    }
    return status;
}

AkmECompass::Status AK8963::getData(char *buf) {
    AkmECompass::Status status = AkmECompass::ERROR;
    
    if ((status=AK8963::read(AK8963_REG_ST1, buf, AK8963_BUF_LENGTH_BDATA)) != AkmECompass::SUCCESS) {
        // I2C read failed.
        return status;
    }
    
    return status;
}

AkmECompass::Status AK8963::getOperationMode(AkmECompass::Mode *mode){
    AkmECompass::Status status = AkmECompass::ERROR;
    char buf = 0;
    if ((status=AK8963::read(AK8963_REG_CNTL1, &buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        // I2C read failed.
        return status;
    }    
    mode->mode = (AkmECompass::OperationMode)(buf & AK8963_BIT_MASK_MODE);    
    return status;    
}

AkmECompass::Status AK8963::setOperationMode(AkmECompass::Mode mode){
    AkmECompass::Status status = AkmECompass::ERROR;
    
    // The device has to be put into power-down mode first before switching mode.
    char buf = AkmECompass::MODE_POWER_DOWN;
    if (mode.mode != AkmECompass::MODE_POWER_DOWN) {
        if ((status=AK8963::write(AK8963_REG_CNTL1, &buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
            // I2C write failed.
            return status;
        }
        wait_us(AK8963_WAIT_POWER_DOWN_US);
    }

    // Switch to the specified mode.
    buf = (mode.mode | AK8963_CNTL1_OUTPUT_16BIT);    
    if ((status=AK8963::write(AK8963_REG_CNTL1, &buf, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        // I2C write failed.
        return status;
    }
    
    wait_us(AK8963_WAIT_POWER_DOWN_US);
    
    return AkmECompass::SUCCESS;
}


AkmECompass::Status AK8963::getMagneticVectorLsb(MagneticVectorLsb *lsb) {
    AkmECompass::Status status = AkmECompass::ERROR;
    char buf[AK8963_BUF_LENGTH_BDATA];
    
    if ((status=AK8963::getData(buf)) != AkmECompass::SUCCESS) {
        // Failed to get data.
        return status;
    }
    
    // Checks sensor overflow
    if ((buf[7] & AK8963_BIT_MASK_HOFL) > 0) {
        // Magnetic sensor overflow
        lsb->isOverflow = true;
    } else {
        // Magnetic sensor didn't overflow.
        lsb->isOverflow = false;
        // Calculates magnetic field value
        int16_t xi = (int16_t)((buf[2] << 8) | buf[1]);
        int16_t yi = (int16_t)((buf[4] << 8) | buf[3]);
        int16_t zi = (int16_t)((buf[6] << 8) | buf[5]);
        lsb->lsbX = (int32_t)(xi * ((sensitivityAdjustment.x + 128)/AK8963_DENOMINATOR) );
        lsb->lsbY = (int32_t)(yi * ((sensitivityAdjustment.y + 128)/AK8963_DENOMINATOR) );
        lsb->lsbZ = (int32_t)(zi * ((sensitivityAdjustment.z + 128)/AK8963_DENOMINATOR) );
        lsb->lsbTemp = 0x00;
    }
    return AkmECompass::SUCCESS;
}

AkmECompass::Status AK8963::getMagneticVector(MagneticVector *vec) {
    AkmECompass::Status status = AkmECompass::ERROR;
    MagneticVectorLsb lsb;
    status = getMagneticVectorLsb(&lsb);
    if(status == AkmECompass::SUCCESS){
        vec->isOverflow = lsb.isOverflow;
        vec->mx = (float)( lsb.lsbX * AK8963_SENSITIVITY );
        vec->my = (float)( lsb.lsbY * AK8963_SENSITIVITY );
        vec->mz = (float)( lsb.lsbZ * AK8963_SENSITIVITY );
        vec->temp = 0.0;
    }else{
        return status;    
    }
    return AkmECompass::SUCCESS;
}

AkmECompass::Status AK8963::getSensitivityAdjustment(SensitivityAdjustment *sns) {
    AkmECompass::Status status = AkmECompass::ERROR;
    
    // Set the device into the fuse ROM access mode.
    char data = AK8963_CNTL1_MODE_FUSE_ROM;
    if ((status=AK8963::write(AK8963_REG_CNTL1, &data, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        // I2C write failed.
        return status;
    }
    
    // Wait
    wait_us(AK8963_WAIT_POWER_DOWN_US);
    
    // Read the fuse ROM
    char buf[AK8963_BUF_LENGTH_ASA];
    if ((status=AK8963::read(AK8963_REG_ASAX, buf, AK8963_BUF_LENGTH_ASA)) != AkmECompass::SUCCESS) {
        // I2C read failed.
        return status;
    }
    
    sns->x = buf[0];
    sns->y = buf[1];
    sns->z = buf[2];
    
    // Set the device into the power-down mode
    data = AK8963_CNTL1_MODE_PDN;
    if ((status=AK8963::write(AK8963_REG_CNTL1, &data, AKMECOMPASS_LEN_ONE_BYTE)) != AkmECompass::SUCCESS) {
        // I2C write failed.
        return status;
    }
    
    // Wait
    wait_us(AK8963_WAIT_POWER_DOWN_US);
    
    return AkmECompass::SUCCESS;  
}

