#ifndef AK8963_H
#define AK8963_H

#include "mbed.h"
#include "akmecompass.h"

/**
 * This is a device driver of AK8963.
 *
 * @note AK8963 is a 3-axis magnetometer (magnetic sensor) device manufactured by AKM.
 * 
 * Example:
 * @code
 * #include "mbed.h"
 * #include "ak8963.h"
 * 
 * #define I2C_SPEED_100KHZ    100000
 * #define I2C_SPEED_400KHZ    400000
 * 
 * int main() {
 *     // Creates an instance of I2C
 *     I2C connection(I2C_SDA0, I2C_SCL0);
 *     connection.frequency(I2C_SPEED_100KHZ);
 *
 *     // Creates an instance of AK8963
 *     AK8963 ak8963(&connection, AK8963::SLAVE_ADDR_1);
 *     
 *     // Checks connectivity
 *     if(ak8963.checkConnection() != AK8963::SUCCESS) {
 *         // Failed to check device connection.
 *         // - error handling here -
 *     }
 *     
 *     // Puts the device into continuous measurement mode.
 *     AkmECompass::Mode mode;
 *     mode.mode = AK8963::MODE_CONTINUOUS_1;
 *     if(ak8963.setOperationMode(mode) != AK8963::SUCCESS) {
 *         // Failed to set the device into continuous measurement mode.
 *         // - error handling here -
 *     }
 *
 *     while(true) {
 *         // checks DRDY
 *         if (statusAK8963 == AK8963::DATA_READY) {
 *             AK8963::MagneticVector mag;
 *             ak8963.getMagneticVector(&mag);
 *             // You may use serial output to see data.
 *             // serial.printf("%d,%5.1f,%5.1f,%5.1f\n",
 *             //    mag.isOverflow,
 *             //    mag.mx, mag.my, mag.mz);
 *             statusAK8963 = AK8963::NOT_DATA_READY;
 *         } else if (statusAK8963 == AK8963::NOT_DATA_READY) {
 *             // Nothing to do.
 *         } else {
 *             // - error handling here -
 *         }
 *     }
 * }
 * @endcode
 */
class AK8963 : public AkmECompass
{
public:

    /**
     * Constructor.
     *
     */
    AK8963();
        
    /**
     * Destructor.
     *
     */
    virtual ~AK8963();

    /**
     * Initialize sensor with I2C connection.
     *
     * @param _i2c Pointer to the I2C instance
     * @param addr Slave address of the device
     * @param id Device Id enum
     *
     */
    virtual void init(I2C *i2c, SlaveAddress addr, DeviceId deviceId);

    /**
     * Initialize sensor with SPI connection.
     *
     * @param _spi Pointer to the SPI instance
     * @param _cs Pointer to the chip select DigitalOut pin
     * @param deviceId Device Id enum
     *
     */
    virtual void init(SPI *_spi, DigitalOut *_cs, DeviceId deviceId);
    
    /**
     * Check the connection. 
     *
     * @note Connection check is performed by reading a register which has a fixed value and verify it.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status checkConnection();

    /**
     * Gets device operation mode.
     *
     * @param mode device opration mode
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status getOperationMode(Mode *mode);

    /**
     * Sets device operation mode.
     *
     * @param mode device opration mode
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status setOperationMode(Mode mode);

    /**
     * Check if data is ready, i.e. measurement is finished.
     *
     * @return Returns DATA_READY if data is ready or NOT_DATA_READY if data is not ready. If error happens, returns another code.
     */
    virtual AkmECompass::Status isDataReady();

    /**
     * Gets magnetic vector from the device in LSB.
     *
     * @param lsb Pointer to a instance of MagneticVectorLsb
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status getMagneticVectorLsb(MagneticVectorLsb *lsb);

    /**
     * Gets magnetic vector from the device in uT.
     *
     * @param vec Pointer to a instance of MagneticVector
     *
     * @return Returns SUCCESS when succeeded, otherwise returns another code.
     */
    virtual AkmECompass::Status getMagneticVector(MagneticVector *vec);

private:
    
    /**
     * Holds sensitivity adjustment values
     */
    SensitivityAdjustment sensitivityAdjustment;    
    
    /**
     * Gets magnetic data, from the register ST1 (0x02) to ST2 (0x09), from the device.
     * 
     * @param buf buffer to store the data read from the device.
     * 
     * @return Returns SUCCESS when succeeded, otherwise returns another.
     */
    AkmECompass::Status getData(char *buf);
    
    /**
     * Gets sensitivity adjustment values from the register ASAX, ASAY, and ASAZ.
     *
     * @param buf buffer to store sensitivity adjustment data
     *
     * @return SUCCESS when succeeded, otherwise returns another.
     */
    AkmECompass::Status getSensitivityAdjustment(SensitivityAdjustment *sns);
    
    /**
     * Sub init function.
     *
     */
    void init_sub();
};

#endif
